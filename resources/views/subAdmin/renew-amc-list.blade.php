 @extends('layouts.app')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h2 class="mb-2">View Amc Request </h2>
          
          </div><!-- /.col -->
           
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
       <div class="card">
        <div class="card-body">
          <div class="table-responsive "> 
                <table  class="table stripe row-border order-column example" id="renewList">
                    <thead style="background-color: #2c349c;color: #fff;">
                      <tr>
                          <th>Product Name</th>
                          <th>Request Type</th>
                          <th>Phone</th>
                          <th>Date</th>
                           <th>Status</th>  
                      </tr>
                  </thead>
                  <tbody id="viewAmcRequest">   
                <?php $i =0; foreach($renewlit as $list)  { $i++; ?>
                <tr style="height:50px;"   id="row_<?php echo $list['req_id']; ?>">
                   <td><a  href="#"  onclick="viewTicket('<?php echo $list['req_id']; ?>','<?php echo $list['product_name'] ?>','<?php echo $list['type']; ?>')" data-id="<?php echo $list['req_id']; ?>" ><?php echo $list['product_name'] ?></a></td>
                  <td><?php echo $list['type']; ?></td>
                  <td><?php echo $list['phone']; ?></td>
                  <td><?php echo $list['created_at']; ?></td>
                    <td> 
                     
                       <?php if (is_null($list['read_at'])) { ?>
                        <input data-id="<?php echo $list['req_id']; ?>" class="toggle-class closeOpenTicket disablebtn" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Close" data-off="Open"  data-size="sm" > 
                         <?php }  ?>
                          <?php if(!is_null($list['read_at']))  { ?>
                        <input data-id="<?php echo $list['req_id']; ?>" id="id_<?php echo $list['req_id']; ?>" class="toggle-class closeOpenTicket" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Close" data-off="Open" checked data-size="sm" > 

                        <?php }  ?> 
                     </td> 
                     <div id="message_{{ $list['req_id'] }}" style="display: none;">{{ $list['message'] }}</div>   
                     <div id="read_{{ $list['req_id'] }}" style="display: none;">{{ $list['read_at'] }}</div>   
                      
                 </tr> 
                      <?php } ?>
                                
                 </tbody>
                 
                </table>
                 
                <!-- /.table -->
          </div>
        </div>
        <!-- /.row (main row) -->
       </div><!-- /.container-fluid -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade bg-fusion-900" id="ViewAMCModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid grey; ">
                <h5 class="modal-title" >View Ticket :-<span id="typAMC"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body" >
               <form style="  padding: 10px;">
                  <div class="form-group">
                    <label style="margin-bottom:4px !important;" >Product Name:</label>
                    <input type="text" class="form-control" id="productName">
                  </div>
                  <div class="form-group">
                    <label style="margin-bottom:4px !important;">Message:</label>
                    <textarea class="form-control" id="usermessage" rows="6" cols="80"></textarea>
                  </div>
                </form>
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
             </div>
             
        </div>
    </div>
</div>
@endsection
  
