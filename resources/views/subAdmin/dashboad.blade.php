@extends('layouts.app')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
               <h1 class="mb-3">Welcome {{ Auth::user()->name }}</h1>
          </div><!-- /.col -->
          <!-- /.col --> 
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div> 
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{Session::get('success')}}
            </div>
        @elseif(Session::has('failed'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{Session::get('failed')}}
            </div>
        @endif

        @if(auth()->user()->approve == "Success") 
        <div class="row">
             <div class="col-md-3 stretch-card grid-margin">
                <div class="card ">
                  <div class="  card-body" style="padding: 2.2rem 2.2rem;">
                   <a class="text-primary f1 active"  href="{{ url('userNotification') }}" ><i class="mdi mdi-cellphone-android"></i><span>Send Service Notification</span></a>
                     
                       
                  </div>
                </div>
              </div>
              <div class="col-md-3 stretch-card grid-margin">
                <div class="card ">
                  <div class="text-center card-body" style="padding: 2.2rem 2.2rem;">
                    <a class="text-primary f1" href="{{ url('oemAdmin/ViewServiceRequest') }}"><i class="mdi mdi-autorenew"></i> <span>Renew AMC</span></a>
                  </div>
                  
                </div>
              </div> 
              <div class="col-md-3 stretch-card grid-margin">
                <div class="card ">
                  <div class="text-center  card-body" style="padding: 2.2rem 2.2rem;">
                    <a class="text-primary f1" href="{{ url('oemAdmin/ViewRenewAmc') }}"><i class="mdi mdi-eye"></i>  <span>View Service Request</span></a>
                  </div>
                  
                </div>
              </div> 
         </div>

         @else
            <div class="row">
             <div class="col-md-12 stretch-card grid-margin">
                <div class="card ">
                  <div class="  card-body" style="padding: 2.2rem 2.2rem;">
                   <h5 class="text-primary   active"  > <span>Please Wait For Admin Permissions</span></h5>
                     
                       
                  </div>
                </div>
              </div>
            </div>

         @endif
     </div> 
    </section>
    <!-- /.content -->
  </div>
@endsection
