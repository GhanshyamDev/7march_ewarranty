<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('User Login') }}</title>
 
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/icheck-bootstrap/icheck-bootstrap.min.css') }}">
   <link rel="stylesheet" href=" {{ asset('assets/css/style.css') }}">
    <!-- Scripts -->
 
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<style>
    .bs-example{
      margin: 20px;
        position: relative;
    }
</style>
<script>
    $(document).ready(function(){
        $(".show-toast").click(function(){
            $("#myToast").toast('show');
        });
    });
</script>
    <!-- Styles -->
   <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body >
  <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5" style="border: 1px solid #eae6e6e0;">
                <div class="brand-logo text-center">
                  <img src="{{ asset('img/clyfe.png') }}">
                </div>
               
                <form method="POST" action="{{ route('login') }}">
                     @csrf
                     <div class="form-group mb-1">
                        <input type="email" class="form-control @error('email') is-invalid @enderror"   name="email" value="{{ old('email') }}" placeholder="Enter Email" required autocomplete="email" autofocus>
                        
                       </div>
                        @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                           
                      <div class="form-group pt-3 mb-1">
                       <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter Password" required autocomplete="current-password">
                         
                      </div>
                      @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                      <div class="row">
                        <div class="col-8">
                         <!--  <div class="icheck-primary ml-3">
                              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                             
                            <label for="remember ">
                              {{ __('Remember Me') }}
                            </label>
                          </div> -->
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                           
                        </div>

                        <!-- /.col -->
                      </div>
                      <div class="social-auth-links text-center mt-2 mb-3">
                       <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btnk" >  {{ __('Sign In') }}  </button>
                      </div>
                       @guest
                             @if (Route::has('register'))
                               <p class="mb-0"><a href="{{ route('register') }}" class="text-center" >{{ __('Register') }}</a></p>
                              @endif
                              @if (Route::has('password.request'))
                             <!--  <a href="{{ route('password.request') }}" >
                                  {{ __('Forgot Your Password?') }}
                              </a> -->
                              @endif
                              
                             @endguest
                    </form>
 
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
  <div class="toast" id="myToast" style="position: absolute; top: 0; right: 0;">
    <div class="toast-header">
        <strong class="mr-auto"><i class="fa fa-grav"></i> We miss you!</strong>
        <small>11 mins ago</small>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">
        <div>It's been a long time since you visited us. We've something special for you. <a href="#">Click here!</a></div>
    </div>
</div>
<script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src=" {{ asset('dist/js/adminlte.js') }}"></script>
</body>
</html>
