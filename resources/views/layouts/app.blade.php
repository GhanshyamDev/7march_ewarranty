  <!DOCTYPE html>
<html lang="en"> 
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Clyfe</title>
    @notifyCss 
    <!-- plugins:css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"   >
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
    <!-- Start datatables -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> 
     <link rel="stylesheet" href="{{ asset('assets/dataTable/dataTables.bootstrap4.min.css') }}"> 
     <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css"> 

 <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">


       <!-- End  datatables --> 
    <!-- <link rel="stylesheet" href=" {{ asset('assets/css/bootstrap.min.css') }}"> -->
    <link rel="stylesheet" href=" {{ asset('assets/css/style.css') }}">
    <!-- End layout styles -->
   
    <link rel="stylesheet" href="{{ asset('assets/select2/css/select2.min.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
   <style>
    .overlay{
      display: none;
      position: fixed;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      z-index: 999;
      background: rgba(255,255,255,0.8) url({{ asset("img/loading.gif") }}) center no-repeat;
}
 
/* Turn off scrollbar when body element has the loading class */
body.loading{
  overflow: hidden;   
}
/* Make spinner image visible when body element has the loading class */
body.loading .overlay{
  display: block;
}
button.dt-button, .button {
    position: relative;
    display: inline-block;
    box-sizing: border-box;
    margin-right: 0.333em;
    margin-bottom: 0.333em;
    padding: 0.5em 1em;
    border: 1px solid #2c349c;
    border-radius: 2px;
    cursor: pointer;
    font-size: 0.88em;
    line-height: 1.6em;
    color: #fff;
    white-space: nowrap;
    overflow: hidden;
   background-color: #2c349c!important;
    background: -webkit-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
    background: -moz-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
    background: -ms-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
    background: -o-linear-gradient(top, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
    background: linear-gradient(to bottom, rgba(230,230,230,0.1) 0%, rgba(0,0,0,0.1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,StartColorStr='rgba(230, 230, 230, 0.1)', EndColorStr='rgba(0, 0, 0, 0.1)');
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    text-decoration: none;
    outline: none;
    text-overflow: ellipsis;
}
</style>
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center" style="border-bottom:1px solid #eaeaea9c;">
          @if(auth()->user()->is_admin == 1) 
       <a class="navbar-brand brand-logo"  href="#"><img src="{{ asset('img/clyfe.png')  }}" ></a>
          <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('img/favicon.png') }}" alt="logo" /></a>
          @else 
                  @if(auth()->user()->avatar == "NULL") 
            <a class="navbar-brand brand-logo" href="#"><img src="{{ asset('img/category') }}/{{ Auth::user()->avatar }}" ></a>
          <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('img/favicon.png') }}" alt="logo" /></a>
              @else 
         <a class="navbar-brand brand-logo" href="#"><img src="{{ asset('img/avatar.png') }} " style="width: calc(90px - 20px) !important;height: 60px !important;"></a>
          <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('img/avatar.png') }}" alt="logo"  style="width: calc(90px - 60px);height: 20px;" /></a>
             @endif
          @endif 
          
       
    
    

        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="search-field d-none d-md-block">
            <form class="d-flex align-items-center h-100" action="#">
               
            </form>
          </div>
          <ul class="navbar-nav navbar-nav-right">
           
             <li class="nav-item dropdown">
                @if(auth()->user()->is_admin == 1) 

                @else
                <img src="{{ asset('img/clyfe.png') }}" style="width: 120px;">
                @endif
               
               
            </li>
            <li class="nav-item dropdown">
             <!--  <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                <i class="mdi mdi-bell-outline"></i>
                <span class="count-symbol bg-danger"></span>
              </a>
                -->
            </li>
             
             <li class="nav-item nav-profile dropdown">
              <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <!-- <div class="nav-profile-img">
                  <img src="{{ asset('assets/images/faces/face1.jpg') }}" alt="image">
                  <span class="availability-status online"></span>
                </div> -->
                <div class="nav-profile-text">
                  <p class="mb-1 text-black">{{ Auth::user()->name }}</p>
                </div>
              </a>
              <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                <a class="dropdown-item" href="{{ url('oemAdmin/my-account')}}">
                  <i class="mdi mdi-cached mr-2 text-success"></i> My Account</a>
                <div class="dropdown-divider"></div>
                 
                <a class="dropdown-item"href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
               <i class="mdi mdi-logout mr-2 text-primary"></i>  {{ __('Logout') }}
                </a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf    </form>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
          @if(auth()->user()->approve != "Pending")   
            @if(auth()->user()->is_admin == 1) 
             
              <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/dashboard') }}">
                  <span class="menu-title">Dashboard</span>
                  <i class="mdi mdi-home menu-icon"></i>
                </a>
             </li>
 
            @else
              
            <li class="nav-item">
              <a class="nav-link" href="{{ route('home') }}">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="{{ url('oemAdmin/createCategory') }}">
                <span class="menu-title">Create Category</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

             <li class="nav-item">
              <a class="nav-link" href="{{ url('oemAdmin/createGeography') }}">
                <span class="menu-title">Create Geography </span><i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

             <li class="nav-item">
              <a class="nav-link"  href="{{ route('oemAdmin.userCreate') }}">
                <span class="menu-title">Create User </span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>
 
           <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/viewTicket') }}">
                <span class="menu-title">View Ticket </span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>
 

            <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/ViewRenewAmc') }}">
                <span class="menu-title">Renew AMC</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

             <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/ViewServiceRequest') }}">
                <span class="menu-title">View Service Request</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>
         <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/report') }}">
                <span class="menu-title">Report</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>
            @endif
             <li class="nav-item">
                <a class="nav-link" href="{{ url('userNotification') }}">
                  <span class="menu-title">Notification</span>
                  <i class="mdi mdi-arrow-right menu-icon"></i>
                </a>
             </li>
         @endif  
          </ul>

        </nav>
        <!-- partial -->
        <div class="main-panel">
        
            @yield('content')
        </div>


        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>

<!--     //Modal --> 


<!-- medium modal -->

  <div class="overlay"></div>
<script src="{{ asset('assets/jquery/jquery-3.5.1.js') }}"></script> 
<script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('assets/js/off-canvas.js') }}"></script>
<script src="{{ asset('assets/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('assets/js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
<script src="{{ asset('assets/js/file-upload.js') }}"></script>
<script src="{{ asset('assets/adminScript/admin.js') }}"></script>
 <script src="{{ asset('assets/select2/js/select2.full.min.js') }}"></script>
 <script src="{{ asset('assets/dataTable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dataTable/dataTables.bootstrap4.min.js') }}"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.semanticui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>  
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
 

<script type="text/javascript">

$(document).ready(function() {
        var table = $('.example').DataTable( {
         dom: 'Bfrtip',
          lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ], 
          responsive: true,
          orderMulti:false,
          buttons: [ 'pageLength'],
        } );  

    $('viewAmcRequest').on('click', 'tr', function () {
        var data = table.row( this ).data();
        alert( 'You clicked on '+data+'\'s row' );
    });
});



    $(document).ready(function() {
        var table = $('#example').DataTable( {

            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        } );
     
        table.buttons().container()
              .insertBefore( '#example_filter' );
    });
   



   $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    $('.selectRequest').select2({
      'minimumsearchresult': -1, // only start searching when the user has input 3 or more characters
    });
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
})
</script>
 
  
    <script type="text/javascript">
      $(function() {

    $('.ApproveUser').change(function() {

        var value = $(this).prop('checked') == true ? 'Success' : 'Pending'; 
        var oemuid = $(this).data('id');
         $("#approveMsg").html(" ");
          var confrm = confirm("Are You Sure...!");
     
    if(confrm == true) {
         $.ajax({
             type: "POST",
             url: '{{ url('approveUser') }}',
             data: {'_token': '{{ csrf_token() }}', 'oemuid': oemuid, 'value': value},
            success: function(response){
                if(response['success'] == true ){
                     if(value == 'Success') { var stats = 'Active';} else{  var stats = 'InActive'; }
                  $("#approveMsg").html('<div class="alert alert-success"><strong>'+stats+'!</strong>  User '+stats+' Successfully</div>');
                } else {
                    $("#approveMsg").html(' <div class="alert alert-success"> <strong>Failed!</strong> '+response['data']+' </div>');
                }
            }
       });
     }
    })
  })
 
  // view ticket
  
function viewTicket($id, $productName, $type){
    if($type == "EXTENDED_WARRANTY") {
      var typ ="Extend Warranty";
    } 
    if($type == "AMC") {
      var typ ="AMC";
    }
          let type = $("#typAMC").text(typ);
         let message = $('#message_'+$id).text();  
         let read = $('#read_'+$id).text();  
         $('#ViewAMCModal').modal("show");
           $('#productName').val($productName);  
           $('#usermessage').val(message);  
             
      }
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
         $('.exampleButtons').DataTable({
             dom: 'Bfrtip',
             buttons: [ 'excel'],

         });
    } );

</script>  
 
<script type="text/javascript">
   function sendMarkTicket($id) {
 
       $.ajax({
            url: "{{ route('sendMarkTicket') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               ticket_id:$id
             },
            success:function(response){
               $("#row_"+$id).remove();
               alert(respons.data);
               //$(this).parents('div.alert').remove();
            },
           }); 
    }
      $(function() {
        $('.closeOpenTicket').change(function() {
            var val = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ url('opencloseTicket') }}",
                data: {'_token': '{{ csrf_token() }}', 'id': id, 'value': val},
                success: function(response){

                  if(response['success'] == 200 ){
                   
                    $('.disablebtn').prop('disabled', true);

                  }else {
                     alert(response['success']);
                  }
                     
                  
                }
            });
        })
      }) 
  function replyTicket($id, $type){
      
          $('#replyTicketModal').modal("show");
           $('#ticketID').val($id);  
           $('#tickettype').text($type);  
           $('#ticketMSG').text($('#row_'+$id).data('mesg'));  
  }
 // $("select[name='id_type']").change(function(){
 //      var id_type = $(this).val();
 //      alert(id_type);
      
 //      $.ajax({
 //          url: "{{ route('select-serviceType') }}",
 //          method: 'POST',
 //         data: {
 //               "_token": "{{ csrf_token() }}",
 //                type:id_type
 //             },
 //          success: function(data) {
 //            alert(data);
 //          }
 //      });
 //  });
  
    $(document).ready(function() {

       filterRequestData();
     
    });


    function filterRequestData(){
      var recruitmentTable = null;
     var fromdate = $("#fromdate").val();
     var fromto = $("#fromto").val();
     var type = $("select[name='id_type']").val();
        $("body").addClass("loading"); 
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('oemAdmin/AjaxServiceRequest') }}",
        data: {'_token': '{{ csrf_token() }}',fromdate:fromdate, fromto:fromto,type:type },
       
        success: function(response){
         $("body").removeClass("loading"); 
           // console.log(response);
           $("#dataTablesfilter").html(" ");
           var row ='';
           var i =0;
          if(response.success == true){   
            
               row +='<table id="dttable" class="table table-hover table-striped example ">';
               row +='<thead style="background-color: #2c349c!important;color: #fff;">';
                 row +='<tr>';
                   row +='<th>User</th>';
                   row +='<th>Product Name</th>';
                   row +='<th>Status</th>';
                 row +='</tr>';
              row +=' </thead>';
              row +=' <tbody>';


            $.each(response.data, function(index, value){

                i++;
             row +='<tr>';   
              row +='<td><a href="#" onclick="getUserRequest_details('+value.req_id+')" data-id="'+value.req_id+'">'+value.name+'</a></td>';
               row +='<td id="RequestProduct_'+value.req_id+'" data-val="'+value.product_name+'" data-typ="'+value.type+'" data-msg="'+value.message+'">'+value.product_name+'</td>';
               if(value.read_at == null){
                row +='<td><span class="badge badge-success">Open</span></td>';
                  
               } else {
                  row +='<td><span class="badge badge-info">Close</span></td>';
               }
             row +='</tr>';   
               
           });
               row +='</tbody>';  
                  row +='</table>';  
             $("#dataTablesfilter").append(row);
               initRecruitmentDataTable();
                
         }
        }
     });  
    }
 function getUserRequest_details($id, $type ){

    $('#getUserRequest_Modal').modal("show");
    if($("#RequestProduct_"+$id).data('typ') == "EXTENDED_WARRANTY") {
      var typ ="Extend Warranty";
    } 
    if($("#RequestProduct_"+$id).data('typ') == "AMC") {
      var typ ="AMC";
    }
    $("#requestType").text(typ);
    $("#RequestId").val($id);
    $("#RequestProduct").val($("#RequestProduct_"+$id).data('val'));
    $("#RequestMessage").val($("#RequestProduct_"+$id).data('msg'));
 }
 
  function closeUserRequest(){
 
  var id = $("#RequestId").val()
  $('#getUserRequest_Modal').modal("hide");
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "{{ url('oemAdmin/markServiceRequest') }}",
        data: {'_token': '{{ csrf_token() }}',id:id },
        // beforeSend: function(){
        //      $("body").addClass("loading"); 
        //   },
        success: function(response){
         if(response.success == true){
             
         } else{
          alert("Network Error");
         }
        },
         
    }); 
  }  


// userNotification Page    
 function notificationTO_user($id, $product, $messgae){

     $('#AdminNotification_modal').modal("show");
      $("#Adminid").val($id);
      if($product == ''){
         $("#Adminproduct").val('For All user');
      }else {
          $("#Adminproduct").val($product);
      }
     
      $("#Adminmessage").val($messgae);
   }

 function SendNotification_modal(){
  $('#SendNotification_modal').modal("show");
 }
 

 var testColumn = 0;
var testCounter = 0;
function initRecruitmentDataTable() {
    recruitmentTable = $('#dttable').DataTable({
 
      dom: 'Bfrtip',
      lengthMenu: [
        [ 10, 25, 50, -1 ],
        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ], 
      responsive: true,
      orderMulti:false,
      /* deferRender: true, 
      destroy:false,*/
      
      buttons: [
        'pageLength', 'copy', 'csv', 'excel', 'print'
      ],
         initComplete: function () {
            this.api().columns().every( function () {
          var column = this;
          testColumn = column;
          var select = $('<select style="max-width:100px;"><option value="">Select '+(testColumn !== null && testColumn !== 'undefined' && testColumn.context[0].aoColumns[testCounter].sTitle !== null && testColumn.context[0].aoColumns[testCounter].sTitle !== 'undefined' ? testColumn.context[0].aoColumns[testCounter].sTitle : "")+'</option></select>')
            .appendTo( $(column.footer()) )
            .on( 'change', function () {
              var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
              );
              column
                .search( val ? '^'+val+'$' : '', true, false )
                .draw();
            } );
          testCounter = testCounter +1;
          column.data().unique().sort().each( function ( d, j ) {
            var x=d.search("</a>")
            if(x>-1)
              d = $(d).html()
            select.append( '<option value="'+d+'">'+d+'</option>')
          } );
        } );
        testCounter = 0;
        }
     } );  
  }

</script>
  @include('notify::messages')
        
  <x:notify-messages />
  @notifyJs
  </body>
</html>
