@extends('layouts.customer')

@section('content') 
 <div class="content-wrapper">
     
   <!-- Main content -->

    <section class="content">
      <div class="container-fluid">
         
        <div class="page-header" style="margin: 1rem 0 0.1rem 0;">
          <h2 class="page-title" style="font-size:25px;">
              Welcome <?php echo session('Customer_logged')['name'];?>
          </h2>
          <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
             <?php  $count =  DB::table('tbl_notification')->whereNull('read_at')->count(); ?>
               <?php if($count > 0 ) {  ?>
                <!-- <div class="alert alert-warning alert-dismissible fade show">
                  <strong>Notification!</strong> <span style="color: black;">{{ $count }}</span> Unread Notification <a href="{{ url('users/allNotification') }}">View</a>
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
               </div> -->
             <?php } ?>
            </ul>
          </nav>
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              
    
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" style="border-bottom: none !important;">
                  <thead style="color: #fff;background-color: #2c349c;border-color: #2c349c;">
                     <tr>
                      <th>Sr NO</th>
                      <th>Product Name</th>
                      <th>Manufacturer</th>
                      <th>Pauchase Date</th>
                      <th>Warranty</th>
                     <!--  <th>View Details</th> -->
                     </tr>
                  </thead>
                
                   <tbody >
                         @if(count($userProduct)) 

                           @foreach ($userProduct as $info)  
                            <tr>
                                <td>{{ $info['user_pr_id'] }}</td>
                                <td><a href="{{ url('users/product') }}/{{ Crypt::encryptString($info['product_id']) }}" >{{ $info['product_name'] }}</a></td>
                                <td> <ul class="list-inline">
                  <li class="list-inline-item">
                      <img alt="Avatar" class="table-avatar" style="width: 40px;" src="{{ asset('img/'.$info['logo'] ) }}">
                  {{ $info['category'] }}</li>  </ul></td>
                                <td>{{ date('d M Y', strtotime($info['pauchase_date']))  }} </td>
                               <?php 
                              $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $info['warranty_start']);
                                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $info['warranty_end']);
                                $diff_in_days = $to->diffInDays($from);
                                if($diff_in_days == 1)
                                  { ?> 
                               <td>{{ $diff_in_days  }} Days <span class="badge badge-warning">Expire Soon</span></td>
                                <?php  }  elseif($diff_in_days == 0) { ?>
                               <td>{{ $diff_in_days  }} Days <span class="badge badge-danger">Expire </span></td>
                                 <?php  }  else{ ?>
                                 <td>{{ $diff_in_days  }} Days <span class="badge badge-success">In_warranty</span></td>
                                <?php } ?>
                             <!--    <td> <a  href="{{ url('users/product') }}/{{ Crypt::encryptString($info['product_id']) }}" class="btn btn-primary btn-xs" href="#"><i class="fas fa-folder"> </i> View</a>             
                               </td> -->
                               
                            </tr>
                               @endforeach
                             @else
                                 
                             @endif 
                        </tbody>
                         
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
