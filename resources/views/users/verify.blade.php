 <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('User Login') }}</title>
 
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Scripts -->
  <!--   <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
   <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <p class="h3"><b>Verify</b> </p>
    </div>
    <div class="card-body">
        <div class="card-header">
               @if(Session::has('success'))
                    <span class="alert alert-success alert-dismissible">
                         
                        {{Session::get('success')}}
                        {{Session::get('id')}}
                    </span>
               
                @endif
        </div>
          
         
       <form method="POST" action="{{ url('users/postVerify') }}">
       @csrf
         
          <div class="form-group">
            <input type="hidden" value="<?php echo session('OTP_Verify')['cust_id']; ?>" name="cust_id">
            <input type="text" class="form-control @error('otp') is-invalid @enderror"   name="otp" value="{{ old('otp') }}"   autocomplete="otp" autofocus placeholder="Enter OTP">
             @error('otp')
            <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
            @enderror
          </div>
        
        <div class="social-auth-links text-center mt-2 mb-3">
         <button type="submit" class="btn btn-primary btn-block" style="width: 80px;">  {{ __('Sign In') }}  </button>
        </div>
      </form>
        <p class="mb-0"><a href="{{ route('register') }}" class="text-center" >{{ __('Register') }}</a></p>
    
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
  
<script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src=" {{ asset('dist/js/adminlte.js') }}"></script>
<script type="text/javascript">
   $("#myform").on("submit", function(){
    $("#pageloader").fadeIn();
  });//submit
 
      </script>
</body>
</html>
