<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('cust_id');
            $table->string('name');
            $table->string('email');
            $table->integer('mobile');
            $table->integer('otp_verify')->nullable();
            $table->string('activate')->nullable();
            $table->string('is_customer')->nullable();
             $table->string('password');
            $table->binary('profile');
            $table->string('city');
            $table->string('state');
            $table->integer('pincode');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
