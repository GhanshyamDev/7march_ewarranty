<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
      protected $table ='tbl_cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id','city_name','state_id', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
 
}
